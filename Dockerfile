# This file is a template, and might need editing before it works on your project.
FROM node:lts-alpine

WORKDIR /app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /app/
RUN yarn install

COPY . /app

# replace this with your application's default port
EXPOSE 8080
CMD [ "yarn", "start" ]
